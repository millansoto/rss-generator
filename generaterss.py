#!/usr/bin/env python3
#encoding: utf-8
# Copyright (C) 2020 José Millán Soto <jmillan (at) kde-espana (dot) org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import xml.etree.ElementTree as ET
import requests
from urllib.parse import quote


creator = 'KDE España'
subject = 'podcast'
logo_url = 'https://www.kde-espana.org/wp-content/uploads/2016/08/cropped-klogo-300x300-1.png'
output_file = 'podcast.xml'

ET.register_namespace('itunes','http://www.itunes.com/dtds/podcast-1.0.dtd')
ET.register_namespace('atom','http://www.w3.org/2005/Atom')
ET.register_namespace('media', "http://search.yahoo.com/mrss/")

url_download = "https://archive.org/advancedsearch.php?q=creator%%3A%%28%s%%29+AND+subject%%3A%%28%s%%29&fl%%5B%%5D=avg_rating&fl%%5B%%5D=backup_location&fl%%5B%%5D=btih&fl%%5B%%5D=call_number&fl%%5B%%5D=collection&fl%%5B%%5D=contributor&fl%%5B%%5D=coverage&fl%%5B%%5D=creator&fl%%5B%%5D=date&fl%%5B%%5D=description&fl%%5B%%5D=downloads&fl%%5B%%5D=external-identifier&fl%%5B%%5D=foldoutcount&fl%%5B%%5D=format&fl%%5B%%5D=genre&fl%%5B%%5D=headerImage&fl%%5B%%5D=identifier&fl%%5B%%5D=imagecount&fl%%5B%%5D=indexflag&fl%%5B%%5D=item_size&fl%%5B%%5D=language&fl%%5B%%5D=licenseurl&fl%%5B%%5D=mediatype&fl%%5B%%5D=members&fl%%5B%%5D=month&fl%%5B%%5D=name&fl%%5B%%5D=noindex&fl%%5B%%5D=num_reviews&fl%%5B%%5D=oai_updatedate&fl%%5B%%5D=publicdate&fl%%5B%%5D=publisher&fl%%5B%%5D=related-external-id&fl%%5B%%5D=reviewdate&fl%%5B%%5D=rights&fl%%5B%%5D=scanningcentre&fl%%5B%%5D=source&fl%%5B%%5D=stripped_tags&fl%%5B%%5D=subject&fl%%5B%%5D=title&fl%%5B%%5D=type&fl%%5B%%5D=volume&fl%%5B%%5D=week&fl%%5B%%5D=year&sort%%5B%%5D=date+desc&sort%%5B%%5D=&sort%%5B%%5D=&rows=50&page=1&output=rss&callback=callback&save=yes"

base_et = ET.parse('base_model.xml')
rss_results = ET.fromstring(requests.get(url_download % (quote(creator), quote(subject))).text)


for item in filter(lambda x: x.tag == 'item', rss_results[0]):
    new_item = ET.Element('item')
    print(ET.tostring(item.find('title')))
    new_item.append(item.find('title'))
    new_item.append(item.find('description'))
    new_item.append(item.find('link'))
    new_item.append(item.find('guid'))
    new_item.append(item.find('pubDate'))
    new_item.append(ET.Element('itunes:episodeType'))
    new_item[-1].text = 'full'
    new_item.append(ET.Element('itunes:image'))
    new_item[-1].set('href', logo_url)
    lst = [i for i in (item.find('{http://search.yahoo.com/mrss/}group') or [])]
    if not lst:
        lst = item.findall('{http://search.yahoo.com/mrss/}content')
    lst = [i for i in filter(lambda x: x.get('medium') == 'audio', lst)]
    if lst:
        url = lst[0].get('url')
        response = requests.head(url, allow_redirects=True)
        size = response.headers.get('content-length', -1)
        enclosure_element = ET.Element('enclosure')
        enclosure_element.set('url', url)
        enclosure_element.set('type', lst[0].get('type'))
        enclosure_element.set('length', size)
        new_item.append(enclosure_element)
        base_et.getroot()[0].append(new_item)

base_et.write(output_file)
